<?php
/**
 * Loop Price
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/loop/price.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see 	    https://docs.woocommerce.com/document/template-structure/
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     1.6.4
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

global $product;
$role=wp_get_current_user();
?>

<?php if ( $price_html = $product->get_price_html() ) : ?>
	<h4>
		<?php 

if ((is_user_logged_in() && in_array("wwp_wholesaler", $role->roles))) {
	$tickets = new WC_Product_Variable( $product->get_id());
	$available_variations = $tickets->get_available_variations();
	$variation_id=$available_variations[0]['variation_id'];
	$variable_product1= new WC_Product_Variation( $variation_id );
	$regular_price = $variable_product1 ->regular_price/2;
    $wholesaleprice = $variable_product1 ->wwp_wholesale_amount;
    if($wholesaleprice<>''){
     	echo get_woocommerce_currency_symbol().number_format($wholesaleprice,2);
    } else {			
	 	echo get_woocommerce_currency_symbol().number_format($regular_price,2);
	} 
} else {
	echo woocommerce_price($product->get_price_including_tax());
}





	// 	if ((is_user_logged_in() && !in_array("wwp_wholesaler", $role->roles))) {
	// 		echo woocommerce_price($product->get_price_including_tax());

	
	// } else {	
	// 		$tickets = new WC_Product_Variable( $product->get_id());
	// 		$available_variations = $tickets->get_available_variations();
	// 		$variation_id=$available_variations[0]['variation_id'];
	// 		$variable_product1= new WC_Product_Variation( $variation_id );
	// 		$regular_price = $variable_product1 ->regular_price/2;
 //            $wholesaleprice = $variable_product1 ->wwp_wholesale_amount;
 //            if($wholesaleprice<>''){
 //             	echo get_woocommerce_currency_symbol().number_format($wholesaleprice,2);
 //            } else {			
	// 		 	echo get_woocommerce_currency_symbol().number_format($regular_price,2);
	// 		} 
	// 	} ?>
	</h4>
<?php endif; ?>
