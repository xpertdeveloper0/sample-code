<?php
/**
 * Single Product Price, including microdata for SEO
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/single-product/price.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @author  WooThemes
 * @package WooCommerce/Templates
 * @version 3.0.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

global $product;
$role=wp_get_current_user();
?>
<h1 class="productsingletitle"><?php the_title(); ?></h1>
<p class="price">
	<?php if ( (is_user_logged_in()) && ($role->roles[0]!="wwp_wholesaler")) {?>
	<?php 	echo $product->get_price_html(); // echo woocommerce_price($product->get_price_including_tax()); ?>
	<?php } else { 
		echo $product->get_price_html(); 
	} ?>
</p>
