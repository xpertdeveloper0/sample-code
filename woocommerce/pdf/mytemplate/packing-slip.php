<?php do_action( 'wpo_wcpdf_before_document', $this->type, $this->order ); ?>

<table class="head container">
	<tr>
		<td class="header">
		<?php
		if( $this->has_header_logo() ) {
			$this->header_logo();
		} else {
			echo $this->get_title();
		}
		?>
		</td>
		<td class="shop-info">
			<div class="shop-name">
			<!-- <img width="100" src="http://112.196.4.12/atali/wp-content/uploads/2018/11/final_atali.png">-->
			<img width="100" src="<?php echo home_url(); ?>/wp-content/uploads/2018/11/final_atali.png">
			</div>
			<div class="shop-address"><?php $this->shop_address(); ?></div>
		</td>
	</tr>
</table>

<h1 class="document-type-label">
<?php if( $this->has_header_logo() ) echo $this->get_title(); ?>
</h1>

<?php do_action( 'wpo_wcpdf_after_document_label', $this->type, $this->order ); ?>

<table class="order-data-addresses">
	<tr>
		<td class="address billing-address">
			<h3><?php _e( 'Billing Address:', 'woocommerce-pdf-invoices-packing-slips' ); ?></h3>
			<?php $this->billing_address(); ?>
			<?php if ( isset($this->settings['display_email']) ) { ?>
			<div class="billing-email"><?php $this->billing_email(); ?></div>
			<?php } ?>
			<?php if ( isset($this->settings['display_phone']) ) { ?>
			<div class="billing-phone"><?php $this->billing_phone(); ?></div>
			<?php } ?>
		</td>

		<td class="address shipping-address">
			<!-- <h3><?php _e( 'Shipping Address:', 'woocommerce-pdf-invoices-packing-slips' ); ?></h3> -->
			<?php //if ( isset($this->settings['display_shipping_address'])) { ?>
			<h3><?php _e( 'Ship To:', 'woocommerce-pdf-invoices-packing-slips' ); ?></h3>
			<?php $this->shipping_address(); ?>
			<?php //} ?>
		</td>
		<td class="order-data">
			<table>
				<?php do_action( 'wpo_wcpdf_before_order_data', $this->type, $this->order ); ?>
				<tr class="order-number">
					<th><?php _e( 'Order Number:', 'woocommerce-pdf-invoices-packing-slips' ); ?></th>
					<td><?php $this->order_number(); ?></td>
				</tr>
				<tr class="order-date">
					<th><?php _e( 'Order Date:', 'woocommerce-pdf-invoices-packing-slips' ); ?></th>
					<td><?php $this->order_date(); ?></td>
				</tr>
				<tr class="shipping-method">
					<th><?php _e( 'Shipping Method:', 'woocommerce-pdf-invoices-packing-slips' ); ?></th>
					<td><?php $this->shipping_method(); ?></td>
				</tr>
				<?php do_action( 'wpo_wcpdf_after_order_data', $this->type, $this->order ); ?>
			</table>	





			<table>
				<tr class="shipping-method">
					<td>ABN 30320143242</td>
				</tr>
				<tr class="shipping-method">
					<td>PO Box 5449 Stafford Heights QLD 4053</td>
				</tr>
			</table>





		</td>
	</tr>
</table>

<?php do_action( 'wpo_wcpdf_before_order_details', $this->type, $this->order ); ?>

<table class="order-details">
	<thead>
		<tr>
			<th class="product"><?php _e('Product', 'woocommerce-pdf-invoices-packing-slips' ); ?></th>
			<th class="quantity"><?php _e('Quantity', 'woocommerce-pdf-invoices-packing-slips' ); ?> (<?php echo $this->order->get_item_count();?>)</th>
			<th class="product"><?php _e('Sale Price', 'woocommerce-pdf-invoices-packing-slips' ); ?></th>
		</tr>
	</thead>
	<tbody>
		<?php $items = $this->get_order_items(); if( sizeof( $items ) > 0 ) : foreach( $items as $item_id => $item ) : ?>
		<tr class="<?php echo apply_filters( 'wpo_wcpdf_item_row_class', $item_id, $this->type, $this->order, $item_id ); ?>">
			<td class="product">
				<?php $description_label = __( 'Description', 'woocommerce-pdf-invoices-packing-slips' ); // registering alternate label translation ?>
				<span class="item-name"><?php echo $item['name']; ?></span>
				<?php do_action( 'wpo_wcpdf_before_item_meta', $this->type, $item, $this->order  ); ?>
				<span class="item-meta"><?php echo $item['meta']; ?></span>
				<dl class="meta">
					<?php $description_label = __( 'SKU', 'woocommerce-pdf-invoices-packing-slips' ); // registering alternate label translation ?>
					<?php if( !empty( $item['sku'] ) ) : ?><dt class="sku"><?php _e( 'SKU:', 'woocommerce-pdf-invoices-packing-slips' ); ?></dt><dd class="sku"><?php echo $item['sku']; ?></dd><?php endif; ?>
					<?php if( !empty( $item['weight'] ) ) : ?><dt class="weight"><?php _e( 'Weight:', 'woocommerce-pdf-invoices-packing-slips' ); ?></dt><dd class="weight"><?php echo $item['weight']; ?><?php echo get_option('woocommerce_weight_unit'); ?></dd><?php endif; ?>
				</dl>
				<?php do_action( 'wpo_wcpdf_after_item_meta', $this->type, $item, $this->order  ); ?>
			</td>
			<td class="quantity"><?php echo $item['quantity']; ?></td>
			<td class="product">
				<?php 
$user_id = get_post_meta($this->order->ID, '_customer_user', true);
$user = new WP_User($user_id);
if(in_array('wwp_wholesaler',$user->roles)) {
	$tcal=($item['product']->get_regular_price() * (str_replace("%", "", $item['tax_rates'])))/100;
	$pprice=$item['product']->get_regular_price()+round($tcal,2);
	if($pprice == 0)
		$pprice = $item['item']->get_total()+$item['item']->get_total_tax();
	echo wc_price( $pprice, array( 'currency' => $this->order->get_currency() ) );
} else {
	echo $item['price'];
}
// die();




				// $tcal=($item['product']->get_regular_price() * (str_replace("%", "", $item['tax_rates'])))/100;
				// //echo $item['product']->get_regular_price()+round($tcal,2);
				// echo wc_price( $item['product']->get_regular_price()+round($tcal,2), array( 'currency' => $this->order->get_currency() ) );
				//die();
				//echo $item['price'];
					// $product_id = wc_get_order_item_meta( $item_id, '_product_id', true );
					// $product = new WC_Product($product_id);
				 // 	if( $product->is_on_sale() ) {
				 // 		echo wc_price( $item['item']->get_subtotal_tax()+$product->get_sale_price(), array( 'currency' => $this->order->get_currency() ) );
				 //    } else {
				 //    	echo wc_price( $item['item']->get_subtotal_tax()+$product->get_regular_price(), array( 'currency' => $this->order->get_currency() ) );
				 //    }




				//echo $item['quantity']; 
				?>
				
			</td>
		</tr>
		<?php endforeach; ?>

			
		<tr>
			<td>Total Quantity
			</td>
			<td>

<?php echo $this->order->get_item_count();?>

			</td>
			<td>
			</td>

		</tr>





		<?php endif; ?>
	</tbody>
</table>

<?php do_action( 'wpo_wcpdf_after_order_details', $this->type, $this->order ); ?>

<?php do_action( 'wpo_wcpdf_before_customer_notes', $this->type, $this->order ); ?>
<div class="customer-notes">
	<?php if ( $this->get_shipping_notes() ) : ?>
		<h3><?php _e( 'Customer Notes', 'woocommerce-pdf-invoices-packing-slips' ); ?></h3>
		<?php $this->shipping_notes(); ?>
	<?php endif; ?>
</div>
<?php do_action( 'wpo_wcpdf_after_customer_notes', $this->type, $this->order ); ?>
<!--div class="bankdetails">
<h4>Bank details : </h4>
<p style="margin:0;">Bank Commonwealth</p> 
<p style="margin:0;">Account Name : Atali</p>
<p style="margin:0;">BSB Number : 066153</p>
<p style="margin:0;">Account Number : 10761498</p>
<p style="margin:0;">Please email remittance advice to <a href="malto:hello@atali.com.au">hello@atali.com.au</a></p>
</div-->

<?php if ( $this->get_footer() ): ?>
<div id="footer">
	<?php $this->footer(); ?>
</div><!-- #letter-footer -->
<?php endif; ?>

<?php do_action( 'wpo_wcpdf_after_document', $this->type, $this->order ); ?>