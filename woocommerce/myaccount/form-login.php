<?php
/**
 * Login Form
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/myaccount/form-login.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @author  WooThemes
 * @package WooCommerce/Templates
 * @version 2.6.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

?>

<?php wc_print_notices(); ?>

<?php do_action( 'woocommerce_before_customer_login_form' ); ?>
<?php if ( get_option( 'woocommerce_enable_myaccount_registration' ) === 'yes' ) : ?>
<div class="col-sm-6">
<div class="customer">
<h2><?php _e( 'New Customers', 'woocommerce' ); ?></h2>
<p>Please create an account..</p>

<div class="form-group">
	<?php wp_nonce_field( 'woocommerce-register', 'woocommerce-register-nonce' ); ?>
	<a href="<?=site_url('/registration') ?>" class="btn btn-default"><?php esc_attr_e( 'Create Account', 'woocommerce' ); ?></a>
</div>



</div>
</div>
<?php endif;
if ( get_option( 'woocommerce_enable_myaccount_registration' ) === 'yes' ) :?>

<div class="col-sm-6">
<div class="customer">
<h2><?php _e( 'REGISTERED CUSTOMERS', 'woocommerce' ); ?></h2>
<form class="woocomerce-form- woocommerce-form-login- login-" method="post">
<?php do_action( 'woocommerce_login_form_start' ); ?>

	<div class="form-group">
		<label for="username" class="control-label">
			<?php _e( 'Username or email address', 'woocommerce' ); ?> <em>*</em>
		</label>
		<input type="text" class="form-control woocommerce-Input woocommerce-Input--text input-text" name="username" id="username" value="<?php if ( ! empty( $_POST['username'] ) ) echo esc_attr( $_POST['username'] ); ?>" />
	</div>
	<div class="form-group">
		<label for="password">
			<?php _e( 'Password', 'woocommerce' ); ?> <em>*</em>
		</label>
		<input class="form-control woocommerce-Input woocommerce-Input--text input-text" type="password" name="password" id="password" />
	</div>

	<?php do_action( 'woocommerce_login_form' ); ?>

	<div class="form-group">
		<?php wp_nonce_field( 'woocommerce-login', 'woocommerce-login-nonce' ); ?>
		<input type="submit" class="btn btn-default" name="login" value="<?php esc_attr_e( 'Login', 'woocommerce' ); ?>" />
		<label class="woocommerce-form__label woocommerce-form__label-for-checkbox inline">
			<input class="" name="rememberme" type="checkbox" id="rememberme" value="forever" /> <span><?php _e( 'Remember me', 'woocommerce' ); ?></span>
		</label>
	</div>
	<p class="woocommerce-LostPassword lost_password">
		<a href="<?php echo esc_url( wp_lostpassword_url() ); ?>"><?php _e( 'Lost your password?', 'woocommerce' ); ?></a>
	</p>

<?php do_action( 'woocommerce_login_form_end' ); ?>

</form>



</div>
</div>
<?php endif;?>










<?php do_action( 'woocommerce_after_customer_login_form' ); ?>
